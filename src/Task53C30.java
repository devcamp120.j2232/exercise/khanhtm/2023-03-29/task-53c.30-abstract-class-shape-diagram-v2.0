import s10.*;

public class Task53C30 {
    public static void main(String[] args) throws Exception {
    Rectangle rectangle = new Rectangle(3,7);
    Circle circle = new Circle(1);
    Square square = new Square(6);
    System.out.println(rectangle);
    System.out.println("Dien tich hinh chu nhat: " + rectangle.getArea());
    System.out.println("Chu vi hinh chu nhat: " + rectangle.getPerimeter());
    System.out.println(circle);
    System.out.println("Dien tich hinh tron: " + circle.getArea());
    System.out.println("Chu vi hinh tron: " + circle.getPerimeter());
    System.out.println(square);
    System.out.println("Dien tich hinh vuong: " + square.getArea());
    System.out.println("Chu vi hinh vuong: " + square.getPerimeter());
    }
}
